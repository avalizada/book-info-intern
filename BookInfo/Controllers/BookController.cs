﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookInfo.Models.Repositories;
using BookInfo.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BookInfo.Controllers
{
    public class BookController : Controller
    {
        private IBookRepository _bookRepo;

        public BookController(IBookRepository bookRepo)
        {
            _bookRepo = bookRepo;
        }

        public IActionResult Index()
        {
            List<BookViewModel> books = _bookRepo.Books.Select(x => new BookViewModel {
                Id = x.Id,
                Author = x.Author,
                ISBN = x.ISBN,
                Name = x.Name,
                Price = x.Price,
                Weight = x.Weight,
                Year = x.Year
            }).ToList();

                /*new List<BookViewModel>
            {
                new BookViewModel{ Id = 1, Author = "Cormen", Name="Intorduction to Algorithms", ISBN="ISBN 234234", Price = 30.0, Weight=2.4,Year = 2012},

                new BookViewModel{ Id = 2, Author = "Daniel Kaneman", Name="Thinking fast and slow", ISBN="ISBN 234234", Price = 30.0, Weight=2.4,Year = 2012}
            };*/

            return View(books);
        }
    }
}