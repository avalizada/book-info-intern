﻿using BookInfo.Models.DomainModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookInfo.Models.DomainModels
{
    public class Book : DomainModel
    {
        public string Name { get; set; }

        public string Author { get; set; }

        public int Year { get; set; }

        public string ISBN { get; set; }

        public double Price { get; set; }

        public double Weight { get; set; }
    }
}
