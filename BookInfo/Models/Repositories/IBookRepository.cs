﻿using BookInfo.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookInfo.Models.Repositories
{
    public interface IBookRepository
    {
        IQueryable<Book> Books { get; }
    }
}
